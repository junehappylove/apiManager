package cn.com.xiaoyaoji.utils;

import org.apache.commons.codec.digest.DigestUtils;

import cn.com.xiaoyaoji.Config;

/**
 * @author zhoujingjie created on 2017/7/1
 */
public class PasswordUtils {
	public static String password(String password) {
		return DigestUtils.md5Hex(Config.SALT + password);
	}
}
