package cn.com.xiaoyaoji.task;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zhoujingjie created on 2017/8/29
 */
public class ScheduleManager {
	private static ScheduledExecutorService scheduledExecutorService;
	static Logger logger = LoggerFactory.getLogger(ScheduleManager.class);

	static {
		scheduledExecutorService = Executors.newScheduledThreadPool(2);

	}

	public static void schedule(Runnable runnable, Date first, long period) {
		schedule(runnable, first.getTime() - System.currentTimeMillis(), period);
	}

	public static void schedule(Runnable runnable, long delay, long period) {
		scheduledExecutorService.scheduleWithFixedDelay(runnable, delay, period, TimeUnit.MILLISECONDS);
	}

	public static void shutdown() {
		scheduledExecutorService.shutdownNow();
	}

}
