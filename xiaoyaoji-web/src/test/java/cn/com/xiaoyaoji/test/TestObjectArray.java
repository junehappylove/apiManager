package cn.com.xiaoyaoji.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

import cn.com.xiaoyaoji.util.JdbcUtils;

/**
 * @author zhoujingjie
 *         created on 2017/8/31
 */
public class TestObjectArray {

    @Test
    public void test() throws SQLException {
        Connection connect = JdbcUtils.getConnect();
        String sql ="select name a, status s from project limit 10";
        QueryRunner qr = new QueryRunner();
        List<TestBean> query = qr.query(connect, sql, new BeanListHandler<>(TestBean.class));
        System.out.println(query);
    }
}
